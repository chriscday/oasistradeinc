package com.oasistradetint.oasis;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    private static final int STARTOFBUSINESS = 8;
    private static final int ENDOFBUSINESS = 18;
    // private static final String PHONENUMBER = "18008808648";
    private static final String PHONENUMBER = "18582714440";
    private ServiceInfo service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout layout = (RelativeLayout)inflater.inflate(R.layout.activity_login, null);

        String category = "";
        Intent callingIntent = getIntent();
        if (callingIntent != null) {
            category = callingIntent.getStringExtra("category");
            service = new ServiceInfo(category, getResources(), getPackageName());
        }

        /* Set service-specific content */
        if (service != null) {
            TextView tv = ((TextView) layout.findViewById(R.id.textInfo));
            TextView infoTv = ((TextView) layout.findViewById(R.id.textMoreInfo));
            ImageView iv = ((ImageView) layout.findViewById(R.id.bannerImage));
            setTitle(service.getTitle());
            tv.setText(Html.fromHtml(service.getHeadlineText()));
            infoTv.setText(service.getInfoLinkText());
            iv.setImageResource(service.getBannerID());
        }

        /*  set up phone contact button */
        Button phoneButton = ((Button) layout.findViewById(R.id.buttonCall));

        // TODO: move this to contact screen
        phoneButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + PHONENUMBER));
                startActivity(callIntent);
            }
        });

        /*
        ((Button) layout.findViewById(R.id.buttonSend)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO: move this into its own class at the bottom or into a separate file

                String to = "chriscday@gmail.com";
                String from = "fromemail@test.com";
                String message = "Interested in some service";
                String subject = "New contact from " + from;

                final EditText nameField = (EditText) findViewById(R.id.editTextName);
                //String naam = naamField.getText().toString();
                if(nameField.getText().toString().length() > 0) {
                    to = nameField.getText().toString();
                }

                // android sending email via code is apparently not trivial; punt in this for now.
                Log.i("LoginActivity", "To: " + to + ", from: " + from + ", message: " + message + ", subject: " + subject);

                // TODO: here we should replace form with success message or error message
            }
        });
        */

        setContentView(layout);
        Toolbar toolbar = (Toolbar) (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        // From Login page to back button enabler
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);

        }

        return super.onOptionsItemSelected(item);
    }
}
