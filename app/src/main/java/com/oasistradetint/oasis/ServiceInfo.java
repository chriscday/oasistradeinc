package com.oasistradetint.oasis;

import android.content.res.Resources;

public class ServiceInfo implements ItemInfo {
    String key;
    Resources resources;
    String packageName;

    public ServiceInfo(String key, Resources resources, String packageName) {
        this.key = key;
        this.resources = resources;
        this.packageName = packageName;
    }

    public String getKey() {
        return this.key;
    }

    public String getTitle() {
        return (String) resources.getText(resources.getIdentifier("title_" + this.key, "string", packageName));
    }

    public int getIconID() {
        return resources.getIdentifier("icon_" + this.key, "drawable", packageName);
    }

    public int getBannerID() {
        return resources.getIdentifier("banner_" + this.key + "_grad", "drawable", packageName);
    }

    public String getHeadlineText() {
        return resources.getText(resources.getIdentifier("headline_" + this.key, "string", packageName)).toString();
    }

    public String getInfoLinkText() {
        return resources.getText(resources.getIdentifier("more_info_" + this.key, "string", packageName)).toString();
    }

    public String getSectionText() {
        return resources.getText(resources.getIdentifier("info_" + this.key + "_html", "string", packageName)).toString();
    }
}
