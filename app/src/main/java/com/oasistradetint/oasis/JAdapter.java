package com.oasistradetint.oasis;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Peter on 9/3/2015.
 */
public class JAdapter extends RecyclerView.Adapter<JAdapter.MyViewHolder> {
    public static final int TYPE_NAV = 0;
    public static final int TYPE_DEFAULT = 1;

    private LayoutInflater inflater;
    private Context context;
    private ClickListener clickListener;
    List<ItemInfo> data = Collections.emptyList();
    private int displayType;

    public JAdapter(Context context, List<ItemInfo> data, int displayType) {
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.data=data;
        this.displayType = displayType;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId;
        if (this.displayType == TYPE_NAV) {
            layoutId = R.layout.nav_row;
        } else {
            layoutId = R.layout.custom_row;
        }
        View view=inflater.inflate(layoutId, parent,false);
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ItemInfo current=data.get(position);
        holder.title.setText(current.getTitle());
        holder.icon.setImageResource(current.getIconID());
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener=clickListener;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        ImageView icon;
        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            title = (TextView) itemView.findViewById(R.id.listText);
            icon = (ImageView) itemView.findViewById(R.id.listIcon);

        }

        @Override
        public void onClick(View v) {
            context.startActivity(new Intent(context, LoginActivity.class));
            if(clickListener!=null) {
                clickListener.itemClicked(v, getPosition());
            }
        }
    }

    public interface ClickListener{
        public void itemClicked(View view, int position);
    }
}
