package com.oasistradetint.oasis;

/**
 * Created by Peter on 9/3/2015.
 */
public interface ItemInfo {

    public String getTitle();

    public int getIconID();
}
