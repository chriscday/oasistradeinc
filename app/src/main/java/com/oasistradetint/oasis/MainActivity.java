package com.oasistradetint.oasis;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements JAdapter.ClickListener {

    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private ArrayList<ItemInfo> services;
    private JAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DrawerLayout layout = (DrawerLayout)inflater.inflate(R.layout.activity_main_appbar, null);

        // initialize product list
        services = new ArrayList<ItemInfo>();
        services.add(new ServiceInfo("solar", getResources(), getPackageName()));
        services.add(new ServiceInfo("water",  getResources(), getPackageName()));
        services.add(new ServiceInfo("shade", getResources(), getPackageName()));
        services.add(new ServiceInfo("led", getResources(), getPackageName()));
        services.add(new ServiceInfo("tinting", getResources(), getPackageName()));
        services.add(new ServiceInfo("turf", getResources(), getPackageName()));
        services.add(new ServiceInfo("ac",  getResources(), getPackageName()));

        recyclerView = (RecyclerView) layout.findViewById(R.id.serviceList);

        adapter=new JAdapter(this, services, JAdapter.TYPE_DEFAULT);
        adapter.setClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        setContentView(layout);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer,(DrawerLayout)findViewById(R.id.drawer_layout), toolbar);

    }

    public List<ItemInfo> getNavigationItems(){
        return services;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void itemClicked(View view, int position) {
        ServiceInfo selectedCategory = (ServiceInfo) services.get(position);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("category", selectedCategory.getKey());
        startActivity(intent);

    }
}
