package com.oasistradetint.oasis;

/**
 * Created by Peter on 9/3/2015.
 */
public class Categories implements ItemInfo {
    int iconID;
    String title;

    public Categories(String title, int iconID) {
        this.title = title;
        this.iconID = iconID;
    }

    public String getTitle() {
        return this.title;
    }

    public int getIconID() {
        return this.iconID;
    }
}
