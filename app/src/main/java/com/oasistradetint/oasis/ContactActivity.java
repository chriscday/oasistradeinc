package com.oasistradetint.oasis;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class ContactActivity extends AppCompatActivity {

    private ContactSuccessFragment successFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout layout = (RelativeLayout)inflater.inflate(R.layout.activity_contact_form, null);

        String category = "";
        Intent callingIntent = getIntent();
        if (callingIntent != null) {
            category = callingIntent.getStringExtra("category");
        }

        if (savedInstanceState == null) {
            successFragment = new ContactSuccessFragment();
        }

        ((Button) layout.findViewById(R.id.buttonSend)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO: move this into its own class at the bottom or into a separate file

                boolean hasErrors = false;
                final EditText nameField = (EditText) findViewById(R.id.editTextName);
                final EditText phoneField = (EditText) findViewById(R.id.editTextPhone);
                final EditText emailField = (EditText) findViewById(R.id.editTextMail);
                String nameValue = nameField.getText().toString();
                String emailValue = emailField.getText().toString();
                String phoneValue = phoneField.getText().toString();

                if (nameValue.isEmpty()) {
                    hasErrors = true;
                    nameField.setError(getString(R.string.error_name));
                }

                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailValue).matches()) {
                    hasErrors = true;
                    emailField.setError(getString(R.string.error_email));
                }

                if (!PhoneNumberUtils.isGlobalPhoneNumber(phoneValue)) {
                    hasErrors = true;
                    phoneField.setError(getString(R.string.error_phone));
                }

                if (hasErrors) {
                    return;
                }

                // hide soft keyboard
                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                // android sending email via code is apparently not trivial; punt on this for now.
                Log.i("LoginActivity", "Contact name: " + nameValue);

                LinearLayout contactForm = ((LinearLayout) findViewById(R.id.contactForm));
                contactForm.setVisibility(View.GONE);

                setTitle("Contact Sent");

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                if (successFragment.isAdded()) { // if the fragment is already in container
                    ft.show(successFragment);
                } else {
                    ft.add(R.id.responseContainer, successFragment, "successFragment");
                    ft.commit();
                }
            }
        });


        setContentView(layout);
        Toolbar toolbar = (Toolbar) (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        // From Login page to back button enabler
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);

        }

        return super.onOptionsItemSelected(item);
    }

}
